import marshal, struct, sys, os, time, types, py_compile, webbrowser
from opcode import *
from opcode import __all__ as _opcodes_all

__all__ = ["dis", "disassemble", "distb", "disco",
           "findlinestarts", "findlabels"] + _opcodes_all
del _opcodes_all
_have_code = (types.MethodType, types.FunctionType, types.CodeType,
              types.ClassType, type)
__version__ = 'ALPHA 0.01'

def input2(string):
    
    if sys.version.startswith('2') == True:
        rep = raw_input(string)
    
    else:
        rep = input(string)
    
    return rep

def Exit():
    try:input()
    except:pass
    sys.exit()

def cmdDis(inPath,outPath):
    
    def show_code(code, indent=''):
        bytecode = ''
        bytecode += "{0}code\n".format(indent)
        indent += '   '
        bytecode += "{0}argcount {1}\n".format(indent, code.co_argcount)
        bytecode += "{0}nlocals {1}\n".format(indent, code.co_nlocals)
        bytecode += "{0}stacksize {1}\n".format(indent, code.co_stacksize)
        bytecode += "{0}flags {1}\n".format(indent, code.co_flags)
        bytecode += show_hex("code", code.co_code, indent=indent)
        
        bytecode += "\n{0}DISSTART\n\n".format(indent)
        sys.stdout = open('temp.tmp', 'w')
        dis.disassemble(code)
        sys.stdout = sys.__stdout__
        bytecode += open('temp.tmp').read()
        os.remove('temp.tmp')
        bytecode += "\n{0}DISEND\n\n".format(indent)
        
        bytecode += "{0}consts\n".format(indent)
        for const in code.co_consts:
            if type(const) == types.CodeType:
                show_code(const, indent+'   ')
            else:
                bytecode += "   {0}{1}\n".format(indent, const)
                
        bytecode +=  "{0}names {1}\n".format(indent, code.co_names)
        bytecode +=  "{0}varnames {1}\n".format(indent, code.co_varnames)
        bytecode +=  "{0}freevars {1}\n".format(indent, code.co_freevars)
        bytecode +=  "{0}cellvars {1}\n".format(indent, code.co_cellvars)
        bytecode +=  "{0}filename {1}\n".format(indent, code.co_filename)
        bytecode +=  "{0}name {1}\n".format(indent, code.co_name)
        bytecode +=  "{0}firstlineno {1}\n".format(indent, code.co_firstlineno)
        bytecode += show_hex("lnotab", code.co_lnotab, indent=indent)
        
        return bytecode
        
    def show_hex(label, h, indent):
        code = ''
        h = h.encode('hex')
        if len(h) < 60:
            pass
            code += "{0}{1} {2}\n".format(indent, label, h)
        else:
            code += "{0}{1}\n".format(indent, label)
            for i in range(0, len(h), 60):
                code += "{0}   {1}\n".format(indent, h[i:i+60])
        
        return code+'\n'
    
    byteCode = ''
    f = open(inPath, "rb")
    magic = f.read(4)
    moddate = f.read(4)
    modtime = time.asctime(time.localtime(struct.unpack('L', moddate)[0]))
    byteCode += "magic {0}\n".format(magic.encode('hex'))
    byteCode += "moddate {0} ({1})\n\n".format(moddate.encode('hex'), modtime)
    code = marshal.load(f)
    byteCode += show_code(code)
    
    open(outPath,'w').write(byteCode)

try:
    from PyQt4.QtGui import QMainWindow,QWidget,QApplication,QAction,QGroupBox,QTabWidget,QTextEdit,QFileDialog,QTreeWidget,QLabel,QLineEdit,QPushButton,QListWidget,QMessageBox,QTreeWidgetItem
except ImportError:
    
    print ('WARNING: PyQt4 is not found on this computer, GUI will no be available.\n')
    
    if len(sys.argv) < 3:
        print('''ERROR: Not enough arguments found!\n\nUsage:\nDisassemble a .pyc to a list of opcodes: -d filename output\nCompile a .py script into pyc: -c filename \nPrint the help file: -h\n\nPress enter to exit.''')
        Exit()
        
    if len(sys.argv) == 3:sys.argv.append('')
        
    if sys.argv[1] == '-h':
        print("Py2Bytecode version {0} by dberube4\n\nCommands:\n\n-d\nDissasemble a pyc or pyo file into a .txt file.\n\n-c\nCompile a .py into a .pyc file.\n\n-h\nPrint the help file (this)\n\nUsage:\n-d filename output\n-c filename \n-h\n".format(__version__))
        print('Press enter to exit.')
        Exit()
    
    if not sys.argv[1] in ['-d','-c','-h']:
        print ('ERROR: '+sys.argv[1]+' is not a recognized command.\n\nPress enter to exit.')
        Exit()
    
    if len(sys.argv) == 3 and sys.argv[1] == '-d':
            output = sys.argv[2].split('.')[0]
            output = output + '.txt'
            sys.argv[3] = output
        
    try:
        if sys.argv[1] == '-d':open(sys.argv[2],'rb').read()
        elif sys.argv[1] == '-c':open(sys.argv[2],'r').read()
    except:
        print ('ERROR: Cannot access file " '+sys.argv[2]+' ".\n\nPress enter to exit.')
        Exit()
        
    try:
        if sys.argv[1] == '-d':open(sys.argv[3],'w')
        
        elif sys.argv[1] == '-c':
            name = sys.argv[2].split('.')[0]+'.pyc'
            if os.path.exists(name):
                rep = ''
                while True:
                    rep = input2('This file is already compiled, delete the file? [Y\N]').upper()
                    if rep == 'Y' or rep == 'N':break
                    
                if rep == 'N':
                    print('Compiling cancelled\n\nPress enter to exit.')
                    Exit()
                elif rep == 'Y':
                    os.remove(name)
    except AttributeError:
        print ('ERROR: Cannot access file " '+sys.argv[3]+' ".\n\nPress enter to exit.')
        Exit()
        
    if sys.argv[1] == '-c':
        py_compile.compile(sys.argv[2])
        print ('Compiling done! Press enter to exit.')
        Exit()
    
    elif sys.argv[1] == '-d':
        print ('Dissasembling {0} to {1} ...'.format(sys.argv[2],sys.argv[3]))
        cmdDis(sys.argv[2],sys.argv[3])
        print ('Dissasembling done! Press enter to exit.')
        Exit()

def disassemble(co, lasti=-1):
    """Disassemble a code object."""
    code = co.co_code
    labels = findlabels(code)
    linestarts = dict(findlinestarts(co))
    n = len(code)
    i = 0
    extended_arg = 0
    free = None
    while i < n:
        c = code[i]
        op = ord(c)
        if i in linestarts:
            if i > 0:
                print
            print "%3d" % linestarts[i],
        else:
            print '   ',

        if i == lasti: print ' ',
        else: print '   ',
        if i in labels: print ' ',
        else: print '  ',
        print repr(i).rjust(4),
        print opname[op].ljust(20),
        i = i+1
        if op >= HAVE_ARGUMENT:
            oparg = ord(code[i]) + ord(code[i+1])*256 + extended_arg
            extended_arg = 0
            i = i+2
            if op == EXTENDED_ARG:
                extended_arg = oparg*65536L
            print repr(oparg).rjust(5),
            if op in hasconst:
                print '(' + repr(co.co_consts[oparg]) + ')',
            elif op in hasname:
                print '(' + co.co_names[oparg] + ')',
            elif op in hasjrel:
                print '(to ' + repr(i + oparg) + ')',
            elif op in haslocal:
                print '(' + co.co_varnames[oparg] + ')',
            elif op in hascompare:
                print '(' + cmp_op[oparg] + ')',
            elif op in hasfree:
                if free is None:
                    free = co.co_cellvars + co.co_freevars
                print '(' + free[oparg] + ')',
        print

def findlabels(code):
    """Detect all offsets in a byte code which are jump targets.

    Return the list of offsets.

    """
    labels = []
    n = len(code)
    i = 0
    while i < n:
        c = code[i]
        op = ord(c)
        i = i+1
        if op >= HAVE_ARGUMENT:
            oparg = ord(code[i]) + ord(code[i+1])*256
            i = i+2
            label = -1
            if op in hasjrel:
                label = i+oparg
            elif op in hasjabs:
                label = oparg
            if label >= 0:
                if label not in labels:
                    labels.append(label)
    return labels

def findlinestarts(code):
    """Find the offsets in a byte code which are start of lines in the source.

    Generate pairs (offset, lineno) as described in Python/compile.c.

    """
    byte_increments = [ord(c) for c in code.co_lnotab[0::2]]
    line_increments = [ord(c) for c in code.co_lnotab[1::2]]

    lastlineno = None
    lineno = code.co_firstlineno
    addr = 0
    for byte_incr, line_incr in zip(byte_increments, line_increments):
        if byte_incr:
            if lineno != lastlineno:
                yield (addr, lineno)
                lastlineno = lineno
            addr += byte_incr
        lineno += line_incr
    if lineno != lastlineno:
        yield (addr, lineno)

class GUI(QMainWindow):
    
    def __init__(self):
        super(GUI,self).__init__()
        self.initUI()
        
    def initUI(self):
        
        def initMenuBar():
            self.menubar = self.menuBar()
            
            ##############################################
            self.fileMenu = self.menubar.addMenu('&File')
            
            self.fm_openPYC = QAction('&Open .PYC', self);self.fm_openPYC.triggered.connect(self.menuBarActions)
            
            self.fileMenu.addAction(self.fm_openPYC);
            
            ############################################
            self.opMenu = self.menubar.addMenu('&Options')
            
            self.pyCompile = QAction('&Compile .Py', self);self.pyCompile.triggered.connect(self.menuBarActions)
            
            self.opMenu.addAction(self.pyCompile);
            
            ##############################################
            self.creditMenu = self.menubar.addMenu('&Credit')
            self.credit = QAction('Credit', self);self.credit.triggered.connect(self.menuBarActions)
            
            self.creditMenu.addAction(self.credit);
        
        def initWidget():
            
            self.groupBox1 = QGroupBox('Code Informations',self);self.groupBox1.setGeometry(10,30,210,360)
            self.groupBox1.page = 0
            
            label1 = QLabel('File name:',self.groupBox1);label1.setGeometry(10,20,80,20)
            label2 = QLabel('File size (byte):',self.groupBox1);label2.setGeometry(10,50,80,20)
            label3 = QLabel('Magic number:',self.groupBox1);label3.setGeometry(10,80,80,20)
            label4 = QLabel('Mod date:',self.groupBox1);label4.setGeometry(10,110,80,20)
            label5 = QLabel('Constants:',self.groupBox1);label5.setGeometry(10,140,80,20)
            label6 = QLabel('Names:',self.groupBox1);label6.setGeometry(10,170,80,20)
            label7 = QLabel('Firstlineno:',self.groupBox1);label7.setGeometry(10,200,80,20)
            label8 = QLabel('Argcount:',self.groupBox1);label8.setGeometry(10,230,80,20)
            label9 = QLabel('Nlocals:',self.groupBox1);label9.setGeometry(10,260,80,20)
            label10 = QLabel('Stacksize:',self.groupBox1);label10.setGeometry(10,290,80,20)
            self.nameLE = QLineEdit(self.groupBox1);self.nameLE.setGeometry(90,20,100,20);self.nameLE.setMinimumWidth(100)
            self.lenLE = QLineEdit(self.groupBox1);self.lenLE.setGeometry(90,50,100,20);self.lenLE.setMinimumWidth(100)
            self.magicLE = QLineEdit(self.groupBox1);self.magicLE.setGeometry(90,80,100,20);self.magicLE.setMinimumWidth(100)
            self.modDateLE = QLineEdit(self.groupBox1);self.modDateLE.setGeometry(90,110,100,20);self.modDateLE.setMinimumWidth(100)
            self.cNumLE = QLineEdit(self.groupBox1);self.cNumLE.setGeometry(90,140,100,20);self.cNumLE.setMinimumWidth(100)
            self.nNumLE = QLineEdit(self.groupBox1);self.nNumLE.setGeometry(90,170,100,20);self.nNumLE.setMinimumWidth(100)
            self.fLineN = QLineEdit(self.groupBox1);self.fLineN.setGeometry(90,200,100,20);self.fLineN.setMinimumWidth(100)
            self.argCount = QLineEdit(self.groupBox1);self.argCount.setGeometry(90,230,100,20);self.argCount.setMinimumWidth(100)
            self.Nlocals = QLineEdit(self.groupBox1);self.Nlocals.setGeometry(90,260,100,20);self.Nlocals.setMinimumWidth(100)
            self.Stacksize = QLineEdit(self.groupBox1);self.Stacksize.setGeometry(90,290,100,20);self.Stacksize.setMinimumWidth(100)
            self.leGroup1 = [self.nameLE,self.lenLE,self.magicLE,self.modDateLE,self.cNumLE,self.nNumLE,self.fLineN,self.argCount,self.Nlocals,self.Stacksize,
                             label1,label2,label3,label4,label5,label6,label7,label8,label9,label10]
            
            
            label11 = QLabel('Constants:',self.groupBox1);label11.setGeometry(10,20,80,20);label11.setVisible(False)
            label12 = QLabel('Names:',self.groupBox1);label12.setGeometry(10,160,80,20);label12.setVisible(False)
            self.consListBox = QListWidget(self.groupBox1);self.consListBox.setGeometry(10,45,190,110);self.consListBox.setVisible(False)
            self.nameListBox = QListWidget(self.groupBox1);self.nameListBox.setGeometry(10,185,190,110);self.nameListBox.setVisible(False)
            self.labelGroup2 = [label11,label12,self.consListBox,self.nameListBox]
            
            self.nextBtn = QPushButton('Next',self.groupBox1);self.nextBtn.setGeometry(130,330,70,25);self.nextBtn.clicked.connect(self.actions)
            self.prevBtn = QPushButton('Prev',self.groupBox1);self.prevBtn.setGeometry(10,330,70,25);self.prevBtn.setEnabled(False);self.prevBtn.clicked.connect(self.actions)
            
            self.reprHolder = QTabWidget(self);self.reprHolder.setGeometry(230,30,360,360)
            self.rawDisplay = QTextEdit()
            
            self.formDisplay = QTreeWidget()
            self.formDisplay.setHeaderLabels(['Line Number','Byte Offset','Opcode Name','Operation parameter','Parameter Interpreted','Hex'])
            
            self.reprHolder.addTab(self.rawDisplay,'Raw')
            self.reprHolder.addTab(self.formDisplay,'Formatted')
        
        self.setWindowTitle('PYC Disassembler - by dberube4')
        self.resize(600,400)
        initMenuBar()
        initWidget()
        self.show()
        
    
    def dissasemble(self,inPath):
        
        def parseCode(code,codeObj):
            hexCode = codeObj.encode('hex')
            code = code.split('\n')
            lineNum = 0
            lineItem = None
            for i in code:
                if len(i) > 1:
                    data = i.split(' ')
                    
                    if data[2] != '':
                        lineNum = data[2];data[2] = ''
                        lineItem = QTreeWidgetItem([str(lineNum)])
                        self.formDisplay.addTopLevelItem(lineItem)
                    if data[1] != '':
                        lineNum = data[1];data[1] = ''
                        lineItem = QTreeWidgetItem([str(lineNum)])
                        self.formDisplay.addTopLevelItem(lineItem)
                    if data[0] != '':
                        lineNum = data[0];data[0] = ''
                        lineItem = QTreeWidgetItem([str(lineNum)])
                        self.formDisplay.addTopLevelItem(lineItem)
                    
                    dataList = []
                    for i in data:
                        if i not in ['']:dataList.append(i)
                    
                    dataList.append('');dataList.append('')
                    
                    formattedItem = QTreeWidgetItem(lineItem,['',dataList[0],dataList[1],dataList[2],dataList[3],toHex(hexCode,dataList[0])])

        
        def show_code(code, indent=''):
            bytecode = ''
            self.argCount.setText(str(code.co_argcount))
            self.Nlocals.setText(str(code.co_nlocals))
            self.Stacksize.setText(str(code.co_stacksize))
            
            sys.stdout = open('temp.tmp', 'w')
            disassemble(code)
            sys.stdout = sys.__stdout__
            c = show_hex("code", code.co_code, indent='')+'\n\n'
            parseCode(open('temp.tmp').read(),code.co_code)
            self.rawDisplay.setText(c+open('temp.tmp').read())
            os.remove('temp.tmp')
                    
            self.nameLE.setText(os.path.basename(code.co_filename))
            self.cNumLE.setText(str(len(code.co_consts)))
            self.nNumLE.setText(str(len(code.co_names)))
            self.fLineN.setText(str(code.co_firstlineno))
            
            for i in code.co_consts:
                self.consListBox.addItem(str(i))
            for i in code.co_names:
                self.nameListBox.addItem(str(i))
            
            return bytecode
            
        def show_hex(label, h, indent):
            code = ''
            h = h.encode('hex')
            if len(h) < 60:
                pass
                code += "{0}{1} {2}\n".format(indent, label, h)
            else:
                code += "{0}{1}\n".format(indent, label)
                for i in range(0, len(h), 60):
                    code += "{0}   {1}\n".format(indent, h[i:i+60])
            
            return code+'\n'

        def toHex(code,offset):
            offset = int(offset)*2
            return str(code[offset:offset+6])
        
        byteCode = ''
        f = open(inPath, "rb")
        self.magicLE.setText(str(f.read(4).encode('hex')))
        moddate = f.read(4)
        self.modDateLE.setText(time.asctime(time.localtime(struct.unpack('L', moddate)[0])))
        self.code = marshal.load(f)
        show_code(self.code)
        
        self.lenLE.setText(str(len(open(inPath, "rb").read())))
    
    def menuBarActions(self):
        sender = self.sender()
        
        if sender == self.fm_openPYC:
            self.formDisplay.clear()
            pycPath = QFileDialog.getOpenFileName(self,filter = 'Byte Compiled Python File (*.pyc)')
            if pycPath != '':
                self.dissasemble(pycPath)
            
        if sender == self.pyCompile:
            pyPath = str(QFileDialog.getOpenFileName(self,filter = 'Python File (*.py)'))
            if pyPath != '':
                outPath = pyPath.replace(os.path.basename(pyPath),os.path.basename(pyPath).replace('.py','.pyc'))
                try:
                    py_compile.compile(pyPath,outPath)
                    QMessageBox.information(self,'Compiled','File compiled!')
                except:
                    QMessageBox.critical(self,'Error','Could not compile file.')
            
    def resizeEvent(self,e):
        x = e.size().width()
        y = e.size().height()
        self.groupBox1.setGeometry(10,30,x*0.35,y-40)
        self.reprHolder.setGeometry(x*0.39,30,x*0.58,y-40)
        
        x = self.groupBox1.width()
        self.nameLE.setGeometry(90,20,x-100,20)
        self.lenLE.setGeometry(90,50,x-100,20)
        self.magicLE.setGeometry(90,80,x-100,20)
        self.modDateLE.setGeometry(90,110,x-100,20)
        self.cNumLE.setGeometry(90,140,x-100,20)
        self.nNumLE.setGeometry(90,170,x-100,20)
        self.fLineN.setGeometry(90,200,x-100,20)
        self.argCount.setGeometry(90,230,x-100,20)
        self.Nlocals.setGeometry(90,260,x-100,20)
        self.Stacksize.setGeometry(90,290,x-100,20)
        self.consListBox.setGeometry(10,45,x-20,110)
        self.nameListBox.setGeometry(10,180,x-20,110)
        if x > 170:
            self.nextBtn.setGeometry(x-80,330,70,25)

    def actions(self):
        sender = self.sender()
        
        if sender == self.nextBtn:
            for i in self.leGroup1:
                i.setVisible(False)
            for i in self.labelGroup2:
                i.setVisible(False)
            self.prevBtn.setEnabled(False)
            self.nextBtn.setEnabled(False)
            
            if self.groupBox1.page == 0:
                self.groupBox1.page = 1
                for i in self.labelGroup2:
                    self.prevBtn.setEnabled(True)
                    i.setVisible(True)
        
        if sender == self.prevBtn:
            for i in self.leGroup1:
                i.setVisible(False)
            for i in self.labelGroup2:
                i.setVisible(False)
            self.prevBtn.setEnabled(False)
            self.nextBtn.setEnabled(False)
            
            if self.groupBox1.page == 1:
                self.groupBox1.page = 0
                self.nextBtn.setEnabled(True)
                for i in self.leGroup1:
                    i.setVisible(True)
            

if __name__ == '__main__':
    app = QApplication(sys.argv)
    app.setStyle('plastique')
    gui = GUI()
    sys.exit(app.exec_())